import pytest
import json
import time

from project.server import db
from project.server.models import User, BlacklistToken


def register_user(client, email, password):
    return client.post(
        "/auth/register",
        data=json.dumps(dict(email=email, password=password)),
        content_type="application/json",
    )


def login_user(client, email, password):
    return client.post(
        "/auth/login",
        data=json.dumps(dict(email=email, password=password)),
        content_type="application/json",
    )


def test_user_status(client, init_db):
    resp_register = register_user(
        client, email="joe@example.com", password="test"
    )

    response = client.get(
        "/auth/status",
        headers=dict(
            Authorization="Bearer "
            + json.loads(resp_register.data.decode())["auth_token"]
        ),
    )
    data = json.loads(response.data.decode())
    assert data["status"] == "success"
    assert data["data"] is not None
    assert data["data"]["email"] == "joe@example.com"
    assert data["data"]["admin"] is "true" or "false"
    assert response.status_code == 200


def test_non_registered_user_login(client, init_db):
    """ Test for login of non-registered user """
    response = login_user(client, email="joe@gmail.com", password="123456")
    data = json.loads(response.data.decode())
    assert data["status"] == "fail"
    assert data["message"] == "Invalid email or password."
    assert response.content_type == "application/json"
    assert response.status_code == 404


def test_registered_user_login(client, init_db):
    user = User(email="joe@example.com", password="test")
    init_db.session.add(user)
    init_db.session.commit()
    response = login_user(client, email=user.email, password="test")

    data = json.loads(response.data.decode())
    assert data["status"] == "success"
    assert data["message"] == "Successfully logged in."
    assert data["auth_token"]
    assert response.content_type == "application/json"
    assert response.status_code == 200


def test_registration_with_an_existing_account(client, init_db):
    """ Test registration with already registered email"""
    user = User(email="joe@example.com", password="test")
    init_db.session.add(user)
    init_db.session.commit()
    response = register_user(client, email="joe@example.com", password="cat")
    data = json.loads(response.data.decode())
    assert data["status"] == "fail"
    assert data["message"] == "User already exists. Please Log in."
    assert response.content_type == "application/json"
    assert response.status_code == 202


def test_user_status_malformed_bearer_token(client, init_db):
    """ Test for user status with malformed bearer token"""
    resp_register = register_user(client, "joe@gmail.com", "123456")
    response = client.get(
        "/auth/status",
        headers=dict(
            Authorization="Bearer"
            + json.loads(resp_register.data.decode())["auth_token"]
        ),
    )
    data = json.loads(response.data.decode())
    assert data["status"] == "fail"
    assert data["message"] == "Bearer token malformed."
    assert response.status_code, 401


def test_invalid_logout(client, init_db):
    """ Testing logout after the token expires """
    # user registration
    resp_register = register_user(
        client, email="joe@example.com", password="test"
    )
    data_register = json.loads(resp_register.data.decode())
    assert data_register["status"] == "success"
    assert data_register["message"] == "Successfully registered."
    assert data_register["auth_token"]
    assert resp_register.content_type == "application/json"
    assert resp_register.status_code == 201
    # user login
    resp_login = login_user(client, email="joe@example.com", password="test")

    data_login = json.loads(resp_login.data.decode())
    assert data_login["status"] == "success"
    assert data_login["message"] == "Successfully logged in."
    assert data_login["auth_token"]
    assert resp_login.content_type == "application/json"
    assert resp_login.status_code == 200
    # invalid token logout
    time.sleep(6)
    response = client.post(
        "/auth/logout",
        headers=dict(
            Authorization="Bearer "
            + json.loads(resp_login.data.decode())["auth_token"]
        ),
    )
    data = json.loads(response.data.decode())
    assert data["status"] == "fail"
    assert data["message"] == "Signature expired. Please log in again."
    assert response.status_code == 401


def test_valid_logout(client, init_db):
    """ Test for logout before token expires """
    # user registration
    resp_register = register_user(
        client, email="joe@gmail.com", password="123456"
    )
    data_register = json.loads(resp_register.data.decode())
    assert data_register["status"] == "success"
    assert data_register["message"] == "Successfully registered."
    assert data_register["auth_token"]
    assert resp_register.content_type == "application/json"
    assert resp_register.status_code == 201
    # user login
    resp_login = login_user(client, email="joe@gmail.com", password="123456")
    data_login = json.loads(resp_login.data.decode())
    assert data_login["status"] == "success"
    assert data_login["message"] == "Successfully logged in."
    assert data_login["auth_token"]
    assert resp_login.content_type == "application/json"
    assert resp_login.status_code == 200
    # valid token logout
    response = client.post(
        "/auth/logout",
        headers=dict(
            Authorization="Bearer "
            + json.loads(resp_login.data.decode())["auth_token"]
        ),
    )
    data = json.loads(response.data.decode())
    assert data["status"] == "success"
    assert data["message"] == "Successfully logged out."
    assert response.status_code == 200


def test_registration(client, init_db):
    """Test for user registration"""
    response = register_user(client, email="joe@example.com", password="cat")
    data = json.loads(response.data.decode())
    assert data["status"] == "success"
    assert data["auth_token"]
    assert response.content_type == "application/json"
    assert response.status_code == 201


def test_valid_blacklisted_token_logout(client, init_db):
    """ Test for logout after a valid token gets blacklisted """
    # user registration
    resp_register = client.post(
        "/auth/register",
        data=json.dumps(dict(email="joe@gmail.com", password="123456")),
        content_type="application/json",
    )
    data_register = json.loads(resp_register.data.decode())
    assert data_register["status"] == "success"
    assert data_register["message"] == "Successfully registered."
    assert data_register["auth_token"]
    assert resp_register.content_type == "application/json"
    assert resp_register.status_code == 201
    # user login
    resp_login = client.post(
        "/auth/login",
        data=json.dumps(dict(email="joe@gmail.com", password="123456")),
        content_type="application/json",
    )
    data_login = json.loads(resp_login.data.decode())
    assert data_login["status"] == "success"
    assert data_login["message"] == "Successfully logged in."
    assert data_login["auth_token"]
    assert resp_login.content_type == "application/json"
    assert resp_login.status_code == 200
    # blacklist a valid token
    blacklist_token = BlacklistToken(token=data_login["auth_token"])
    init_db.session.add(blacklist_token)
    init_db.session.commit()
    # blacklisted valid token logout
    response = client.post(
        "/auth/logout",
        headers=dict(
            Authorization="Bearer "
            + json.loads(resp_login.data.decode())["auth_token"]
        ),
    )
    data = json.loads(response.data.decode())
    assert data["status"] == "fail"
    assert data["message"] == "Token blacklisted. Please log in again."
    assert response.status_code == 401


def test_valid_blacklisted_token_user(client, init_db):
    """ Test for user status with a blacklisted valid token """
    resp_register = register_user(
        client, email="joe@gmail.com", password="123456"
    )
    # blacklist a valid token
    blacklist_token = BlacklistToken(
        token=json.loads(resp_register.data.decode())["auth_token"]
    )
    init_db.session.add(blacklist_token)
    init_db.session.commit()
    response = client.get(
        "/auth/status",
        headers=dict(
            Authorization="Bearer "
            + json.loads(resp_register.data.decode())["auth_token"]
        ),
    )
    data = json.loads(response.data.decode())
    assert data["status"] == "fail"
    assert data["message"] == "Token blacklisted. Please log in again."
    assert response.status_code == 401
