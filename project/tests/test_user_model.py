import pytest

from project.server.models import User


def test_decode_auth_token(client, init_db):
    user = User(email="test@test.com", password="test")
    init_db.session.add(user)
    init_db.session.commit()
    auth_token = user.encode_auth_token(user.id)
    assert isinstance(auth_token, bytes)
    assert User.decode_auth_token(auth_token.decode("utf-8")) == 1


def test_encode_auth_token(client, init_db):
    user = User(email="test@test.com", password="test")

    init_db.session.add(user)
    init_db.session.commit()
    auth_token = user.encode_auth_token(1)
    assert isinstance(auth_token, bytes)
