# project/server/tests/test_config.py

import pytest
import os

from flask import current_app
from project.server import app


def test_app_is_testing():
    assert app.config["SECRET_KEY"] is not "my_precious"
    assert app.config["DEBUG"]
    assert (
        app.config["SQLALCHEMY_DATABASE_URI"]
        == "postgresql://scott:tiger@localhost/flask_jwt_auth_test"
    )


def test_app_is_development(dev_app):
    assert app.config["SECRET_KEY"] is not "my_precious"
    assert app.config["DEBUG"] is True
    assert current_app is not None
    assert (
        app.config["SQLALCHEMY_DATABASE_URI"]
        == "postgresql://scott:tiger@localhost/flask_jwt_auth_dev"
    )


def test_app_is_production(prod_app):
    assert app.config["DEBUG"] is False
