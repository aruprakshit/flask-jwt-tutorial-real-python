import pytest

from project.server import app


@app.cli.command()
def test():
    """Run tests."""
    pytest.main(["-v", "--rootdir", "./project/tests"])


if __name__ == "__main__":
    app.run()
